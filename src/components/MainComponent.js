import React, { Component } from 'react';
import Header from "./HeaderComponent";
import Footer from "./FooterComponent";
import Home from "../pages/homepage/home";
import Items from "../pages/checklist/items";
import SignUp from "../pages/users/signup";
import Login from "../pages/users/login";
import {Routes, Route, Outlet, Navigate, useParams} from "react-router-dom";
import { connect } from "react-redux";

const mapState = state => {
    console.log('store start =======>')
    console.log(state)
    console.log('store end =======>')
    return {
        UserInfo: state.UserInfo,
        hasUserInfo: localStorage.getItem('userInfo')!==null?true:false
    }
}

const mapDispatch = (dispatch) => ({
    // userLogout: () => dispatch(User.userLogout(creds)),
    // signIn: (values) => dispatch(User.signIn(values)),
    // signInHideError: () => dispatch(User.signInHideError()),
})

class Main extends Component {
    componentDidMount() {
    }

    render() {
        const Users = () => {
            return (<div><Outlet /></div>)
        };

        const ItemPage = () => {
            let { listId } = useParams();
            return (
                <Items listId={listId}  />
            )
        }

        const MyRoutes = () => {
            if(this.props.hasUserInfo){
                return (
                    <Routes>
                        <Route path="/" element={<Home />}  />
                        <Route path="/checklist/:listId" element={<ItemPage />}></Route>
                        <Route path="/*" element={<Navigate to="/" />} />
                    </Routes>
                )

            }else{
                return (
                    <Routes>
                        <Route path="/" element={<Home />}  />

                        <Route path="/users" element={<Users />} >
                            <Route path="signup" element={<SignUp />} />
                            <Route path="login" element={<Login />} />
                        </Route>

                        <Route path="/*" element={<Navigate to="/" />} />
                    </Routes>
                )
            }
        }

        return (
            <div className="main">
                <Header />
                <MyRoutes />
                <Footer />
            </div>
        );
    }
}

Main = connect(
    mapState,
    mapDispatch
)(Main);

export default Main;
