import React, { Component } from "react";
import {Navbar, NavbarBrand, Nav, NavItem, Button, NavLink} from 'reactstrap';
import { userLogout } from '../pages/users/actions';
import { connect } from "react-redux";

const mapState = state => {
    return {
        UserInfo: state.UserInfo,
        hasUserInfo: localStorage.getItem('userInfo')!==null?true:false,
    }
}

const mapDispatch = (dispatch) => ({
    userLogout: () => dispatch(userLogout()),
})

class Header extends Component{
    render() {
        const UserStatus = (props) => {
            console.log(this.props)
            if(!this.props.hasUserInfo){
                return (
                    <NavLink href="/users/login">
                        <Button outline>
                            <span className="fa fa-user fa-lg p-1"></span> Login
                        </Button>
                    </NavLink>
                )
            }

            return (
                <Button outline onClick={this.props.userLogout}>
                    <span className="fa fa-sign-out fa-lg p-1"></span> Logout
                </Button>
            )
        }

        return(
            <Navbar className="position-fixed top-0 container-fluid" dark expand="md">
                <NavbarBrand className="mr-auto" href="/">
                    <img src="/images/logo.png" width="120" height="40" alt="Online Packing Checklist" />
                </NavbarBrand>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <UserStatus />
                    </NavItem>
                </Nav>
            </Navbar>
        )
    }
}

Header = connect(
    mapState,
    mapDispatch
)(Header);

export default Header;
