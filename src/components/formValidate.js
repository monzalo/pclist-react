import React, { Component } from "react";
import { Input } from 'reactstrap';

class FormValidate extends Component {
    constructor(props) {
        super(props);
        this.state = {targetValue:'', tip:''};
    }

    handleChange = value => {
        let targetValue = value.target.value;
        this.setState({targetValue: targetValue});

        if(targetValue.length === 0){
            this.setState({tip:'Required'});
        }else{
            this.setState({tip:''});
        }
    };

    render() {
            const { setRef } = this.props;
            const { targetValue, tip } = this.state;
            const Tip = () => {
                if(tip!==''){
                    return (<p className="text-danger">{tip}</p>)
                }else{
                    return ('')
                }
            }
            return (
                <>
                    <Input onChange={this.handleChange} value={targetValue} ref={setRef} />
                    <Tip />
                </>
            );
    }
}

export default FormValidate;
