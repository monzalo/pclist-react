import React, { Component } from 'react';
import Main from './components/MainComponent';
import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux';
import './App.scss';
import { InitStore } from './rudex/InitStore';
const store = InitStore();

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <div>
                        <Main />
                    </div>
                </BrowserRouter>
            </Provider>
        );
    }
}

export default App;
