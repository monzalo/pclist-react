import React from 'react';
import {Row, Card, CardHeader, CardBody, Button, NavLink} from 'reactstrap';
import { Field, reduxForm } from 'redux-form';
import { signUp } from '../users/actions';

const validate = values => {
    const errors = {}
    if (!values.username) {
        errors.username = 'Required'
    } else if (values.username.length<3) {
        errors.username = 'Must be longer than 6'
    }

    if (!(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email))) {
        errors.email = 'Invalid email address'
    }

    if (!values.password) {
        errors.password = 'Required'
    } else if (values.password.length<6) {
        errors.password = 'Must be longer than 6'
    }

    if (!values.confirmPassword) {
        errors.confirmPassword = 'Required'
    } else if (values.password!==values.confirmPassword) {
        errors.confirmPassword = 'Password not match'
    }

    return errors
}


const renderField = ({ input, label, placeholder, type, meta: { touched, error, warning } }) => (
    <div className="col m-3">
        <label>{label}</label>
        <div>
            <input {...input} placeholder={placeholder} className="form-control" type={type}/>
            <div className="text-danger">
                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </div>
    </div>
)


const SignUp = (props) => {
    console.log('--- props ---')
    console.log(props)
    console.log('--- props ---')
    const { handleSubmit, submitting, error} = props

    // sign success
    if(props.submitSucceeded){
        // return (
        //     <Navigate to="/users/login" />
        // )

        return (
            <div className="container p-3 p-sm-5">
                <Row className="p-4"></Row>
                <Row>
                    <div className="d-sm-block col col-md"></div>
                    <div className="col col-md-6 d-flex flex-column align-items-center">
                        <img src="/images/icon-succ.svg" className="succ" alt=""/>
                        <h3 className="pt-3">Sign Successful!</h3>
                        <NavLink href="login">click here to login</NavLink>
                    </div>
                    <div className="d-sm-block col col-md"></div>
                </Row>
            </div>
        )
    }

    const Loading = () => {
        if(submitting){
            return(<span className="fa fa-spinner fa-pulse"></span>);
        }else{
            return(<span></span>)
        }
    };

    const ErrorMsg = () => {
        if(error!=null && error!=='undefined'){
            return (
                <div className="alert alert-warning alert-dismissible fade show" role="alert">
                    {error.statusCode} {error.message}
                </div>
            )
        }else{
            return ('')
        }
    };

    return (
        <div className="container p-3 p-sm-5">
            <Row>
                <div className="d-sm-block col col-md"></div>
                <div className="col col-md-6">
                    <Card>
                        <CardHeader className="bg-darkgray text-white">Sign In</CardHeader>
                        <CardBody>
                            <form onSubmit={handleSubmit(signUp)}>
                                <Field name="username" type="text" placeholder="Username" component={renderField} label="Username"/>
                                <Field name="email" type="text" placeholder="Email address" component={renderField} label="Email"/>
                                <Field name="password" type="password" placeholder="Password" component={renderField} label="Password"/>
                                <Field name="confirmPassword" type="password" placeholder="Confirm Password" component={renderField} label="Confirm"/>
                                <div className="m-3">
                                    <Button type="submit" className="btn-signup" disabled={submitting}><Loading /> Submit</Button>
                                </div>
                            </form>
                        </CardBody>
                    </Card>
                </div>
                <div className="d-sm-block col col-md"></div>
            </Row>

            <Row className="text-center text-danger mt-3">
                <ErrorMsg />
            </Row>
        </div>
    )
}

export default reduxForm({
    form: 'signUp',  // a unique identifier for this form
    fields: ['username', 'password'],
    validate
})(SignUp)
