import { SubmissionError } from 'redux-form';
import { apiUrl } from '../../services/config';
import * as ActionTypes from "../../rudex/ActionTypes";
const axios = require('axios');

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

export const signUp = async (values) => {
    var postData = {
        username: values.username,
        password: values.password,
        email: values.email
    }

    await sleep(3000);

    var res = await axios({
        method: 'POST',
        url: apiUrl + 'Customers',
        headers: {'Content-Type':'application/json'},
        data: postData
    }).catch(err => {
        var error = err.response.data.error
        throw new SubmissionError({
            _error: error
        })
    });

    if(res.status === 200){
        console.log('-------- succ --------')
        console.log(res)
        // window.location.href = '/users/login'
        console.log('-------- succ --------')
    }
}

export const userLogin = async (values) => {
    var reg = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
    var postData = {password: values.password}
    if(reg.test(values.username)){
        postData.email = values.username
    }else{
        postData.username = values.username
    }

    await sleep(1000);

    var res = await axios({
        method: 'POST',
        url: apiUrl + 'Customers/login',
        headers: {'Content-Type':'application/json'},
        data: postData
    }).catch(err => {
        localStorage.removeItem('token')
        localStorage.removeItem('userInfo')
        var error = err.response.data.error
        throw new SubmissionError({
            _error: error
        })
    });

    if(res.status === 200){
        console.log('-------- succ --------')
        // save token
        var token = res.data.id
        localStorage.setItem('token', token)

        // get userInfo
        var userInfo = await axios({
            method: 'GET',
            url: apiUrl + 'Customers/' + res.data.userId + '?access_token='+token,
            headers: {'Content-Type':'application/json'}
        }).catch(err => {
            var error = err.response.data.error
            throw new SubmissionError({
                _error: error
            })
        });

        // save userInfo
        localStorage.setItem('userInfo', JSON.stringify(userInfo.data))
    }
}

export const userLogout = () => (dispatch) => {
    dispatch(requestLogout())
    var token = localStorage.getItem('token')
    axios({
        method: 'POST',
        url: apiUrl + 'Customers/logout?access_token='+token,
        headers: {'Content-Type':'application/json'}
    }).catch(err => {
        var error = err.response.data.error
        dispatch(logoutError(error));
    });

    localStorage.removeItem('token');
    localStorage.removeItem('userInfo');
    dispatch(receiveLogout())
}

export const requestLogout = () => {
    return {type: ActionTypes.LOGOUT_REQUEST}
}

export const receiveLogout = () => {
    return {type: ActionTypes.LOGOUT_SUCCESS}
}

export const logoutError = (error) => {
    return {type: ActionTypes.LOGOUT_FAILURE, payload: error}
}
