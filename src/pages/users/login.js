import React from "react";
import { Row, Card, CardHeader, CardBody, Button, NavLink } from 'reactstrap';
import { Navigate } from 'react-router-dom';
import {Field, reduxForm} from "redux-form";
import {userLogin} from '../users/actions';

const validate = values => {
    const errors = {}
    if (!values.username) {
        errors.username = 'Required'
    }

    if (!values.password) {
        errors.password = 'Required'
    }

    return errors
}

const renderField = ({ input, label, placeholder, type, meta: { touched, error, warning } }) => (
    <div className="col m-3">
        <label>{label}</label>
        <div>
            <input {...input} placeholder={placeholder} className="form-control" type={type}/>
            <div className="text-danger">
                {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </div>
    </div>
)

const Login = (props) =>{
    const { handleSubmit, submitting, error } = props

    console.log('--- props ---')
    console.log(props)
    console.log('--- props ---')

    // var userInfo = localStorage.getItem('userInfo')

    if(props.submitSucceeded){
        return (
            <Navigate to="/" />
        )
    }

    const Loading = () => {
        if(submitting){
            return(<span className="fa fa-spinner fa-pulse"></span>);
        }else{
            return(<span></span>)
        }
    };

    const ErrorMsg = () => {
        if(error!=null && error!=='undefined'){
            return (
                <div className="alert alert-warning alert-dismissible fade show" role="alert">
                    {error.statusCode} {error.message}
                </div>
            )
        }else{
            return ('')
        }
    };

    return(
        <div className="container p-3 p-sm-5">

            <Row>
                <div className="d-sm-block col-md"></div>
                <div className="col col-md-6">
                    <Card>
                        <CardHeader className="bg-darkgray text-white">Login</CardHeader>
                        <CardBody>
                            <form onSubmit={handleSubmit(userLogin)}>
                                <Field name="username" type="text" placeholder="Username / Email" component={renderField} label="Username"/>
                                <Field name="password" type="password" placeholder="Password" component={renderField} label="Password"/>
                                <div className="m-3">
                                    <Row>
                                        <div className="col-6">
                                            <NavLink href="/users/signup">Sign up</NavLink>
                                        </div>
                                        <div className="col-6 text-end">
                                            <Button type="submit" className="btn-signup" value="submit" color="primary"
                                                    disabled={submitting}
                                            ><Loading /> Login</Button>
                                        </div>

                                    </Row>
                                </div>
                            </form>
                        </CardBody>
                    </Card>
                </div>
                <div className="d-sm-block col-md"></div>
            </Row>

            <Row className="text-center text-danger mt-3">
                <ErrorMsg />
            </Row>

        </div>
    )

}

export default reduxForm({
    form: 'login',  // a unique identifier for this form
    fields: ['username', 'password'],
    validate,
})(Login)
