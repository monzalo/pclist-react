import { apiUrl } from '../../services/config';
import * as ActionTypes from "../../rudex/ActionTypes";
const axios = require('axios');

let pageInfo = {
    totalNum:0,
    pageNum:0,
    currentPage:1,
    perPage:100
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

export const toggleModal = (modalCurrentStatus) => (dispatch) => {
    return dispatch({
        type: ActionTypes.MODAL_TOGGLE,
        payload: !modalCurrentStatus
    })
}

export const getList = () => async (dispatch) => {
    var token = localStorage.getItem('token')
    var userInfo = localStorage.getItem('userInfo')

    if(!token){
        return dispatch(actionError(null))
    }

    await sleep(1000);

    console.log('--- get checklist ---')
    let filter = JSON.stringify({"order":"createdAt DESC"})
    var res = await axios({
        method: 'GET',
        url: apiUrl + `Customers/${JSON.parse(userInfo).id}/checklists?access_token=${token}&filter=${filter}`,
        headers: {'Content-Type':'application/json'},
    }).catch(err => {
        var error = err.response.data.error
        console.log(error)
        if(error.statusCode === 401){
            localStorage.removeItem('token')
            localStorage.removeItem('userInfo')
            var errMsg = error.statusCode + ' Authentication Failed, please relogin the website'
            return dispatch(actionError(errMsg))
        }
    });

    if(res.status === 200){
        console.log('-------- succ --------')
        console.log(res.data)
        dispatch(receiveList(res.data))
        // window.location.href = '/users/login'
        console.log('-------- succ --------')
    }


}

export const createList = (values) => (dispatch) => {
    var token = localStorage.getItem('token')
    var userInfo = localStorage.getItem('userInfo')
    var uid = JSON.parse(userInfo).id
    var title = values.target.elements[0].value
    if(title.length===0){
        dispatch(actionError('Required'))
    }else{
        dispatch(toggleModal(true))
        dispatch(requestList())

        var postData = {
            title: title,
            customerId: uid
        }
        axios({
            method: 'POST',
            url: apiUrl + 'checklists?access_token='+token,
            headers: {'Content-Type':'application/json'},
            data: postData
        })
            .then( res =>{
                console.log(res)
                dispatch(getList())
            })
            .catch( err => {
                var error = err.response.data.error
                console.log(error)

                if(error.statusCode === 401){
                    localStorage.removeItem('token')
                    localStorage.removeItem('userInfo')
                    var errMsg = error.statusCode + ' Authentication Failed, please relogin the website'
                    dispatch(actionError(errMsg))
                }

            });
    }
}

export const initItems = (listId) => async (dispatch) => {
    var token = localStorage.getItem('token')
    if(!token){return dispatch(actionItemError(null))}

    dispatch(requestItems())

    await sleep(500);

    console.log('--- get items ---')

    var res = await axios({
        method: 'GET',
        url: apiUrl + 'checklists/'+listId+'?access_token='+token,
        headers: {'Content-Type':'application/json'}
    }).catch(err => {
        var error = err.response.data.error
        console.log(error)
        if(error.statusCode === 401){
            localStorage.removeItem('token')
            localStorage.removeItem('userInfo')
            var errMsg = error.statusCode + ' Authentication Failed, please relogin the website'
            return dispatch(actionItemError(errMsg))
        }
    });

    if(res.status === 200){
        console.log('-------- set list info succ --------')
        console.log(res.data)
        var listInfo = {id: res.data.id, title:res.data.title, publish:res.data.publish}
        dispatch(setListInfo(listInfo))
        console.log('-------- set list info succ --------')
    }

    await dispatch(setPageInfo(listId,1))
    await dispatch(getItems(listId))
    // let filter = JSON.stringify({"skip":2,"limit":2})
}

export const setPageInfo = (listId,page) => async (dispatch) => {
    var token = localStorage.getItem('token')
    var res = await axios({
        method: 'GET',
        url: apiUrl + '/checklists/'+listId+'/items/count?access_token='+token,
        headers: {'Content-Type':'application/json'}
    }).catch(err => {
        var error = err.response.data.error
        return dispatch(actionItemError(error.message))
    });

    if(res.status === 200){
        console.log('-------- set page --------')
        console.log(res)
        var count = res.data.count
        var pageNum = Math.ceil(count/pageInfo.perPage);

        pageInfo = {totalNum:count, pageNum:pageNum, currentPage:page, perPage:pageInfo.perPage}
        return dispatch({
            type: ActionTypes.ITEMS_SET_PAGEINFO,
            payload: pageInfo
        })
    }
}

export const updatePageInfo = (countAct) => async (dispatch) => {
    pageInfo.totalNum = pageInfo.totalNum+countAct
    pageInfo.pageNum = Math.ceil(pageInfo.totalNum/pageInfo.perPage);
    return dispatch({
        type: ActionTypes.ITEMS_SET_PAGEINFO,
        payload: pageInfo
    })
}

export const getItems = (listId,page=1) => async (dispatch) => {
    var token = localStorage.getItem('token')

    dispatch(requestItems())

    // var skip = (page-1)*pageInfo.perPage
    // let filter = JSON.stringify({"order":["sort ASC"],"skip":skip,"limit":pageInfo.perPage})

    var res = await axios({
        method: 'GET',
        url: apiUrl + `checklists/${listId}/items?access_token=${token}`,
        headers: {'Content-Type':'application/json'}
    }).catch(err => {
        var error = err.response.data.error
        return dispatch(actionItemError(error.message))
    });

    if(res.status === 200){
        console.log('-------- get items succ --------')
        return dispatch(receiveItems(res.data))
    }
}

export const createNewItem = (item) => async (dispatch) => {
    var token = localStorage.getItem('token')
    var userInfo = localStorage.getItem('userInfo')
    item.customerId = JSON.parse(userInfo).id

    dispatch(requestItems())

    await sleep(300);

    console.log('--- create a new items ---')
    console.log(item)

    var res = await axios({
        method: 'POST',
        url: apiUrl + 'checklists/'+item.checklistId+'/items?access_token='+token,
        headers: {'Content-Type':'application/json'},
        data:item,
    }).catch(err => {
        var error = err.response.data.error
        console.log(error)
        var errMsg = error.statusCode + ' ' + err.message
        return dispatch(actionItemError(errMsg))
    });

    if(res.status === 200){
        console.log('-------- new items succ --------')
        dispatch(getItems(item.checklistId))
        dispatch(updatePageInfo(1))
        console.log('-------- new items succ --------')
    }
}

export const deleteItem = (item, index) => async (dispatch) => {
    var token = localStorage.getItem('token')
    var currentItem = item[index]
    dispatch(requestItems())

    console.log('--- delete item ---')

    await axios({
        method: 'DELETE',
        url: apiUrl + `checklists/${currentItem.checklistId}/items/${currentItem.id}?access_token=${token}`,
        headers: {'Content-Type':'application/json'}
    }).catch(err => {
        var error = err.response.data.error
        var errMsg = error.statusCode + ' ' + err.message
        return dispatch(actionItemError(errMsg))
    });

    item.splice(index,1)
    dispatch({type:ActionTypes.ITEMS_SUCCESS, payload:item})
    dispatch(updatePageInfo(-1))
}

export const insertItemToStore = (items) => async (dispatch) => {
    console.log('--- insert item to store ---')
    dispatch({type:ActionTypes.ITEMS_SUCCESS, payload:items})
}

export const editItemInStore = (items) => async (dispatch) => {
    console.log('--- insert item to store ---')
    dispatch({type:ActionTypes.ITEMS_SUCCESS, payload:items})
}

export const checkItem = (items,listId,item) => async (dispatch) => {
    var token = localStorage.getItem('token')

    console.log('--- check item  ---')
    dispatch({type:ActionTypes.ITEMS_SUCCESS, payload:items})

    axios({
        method: 'PUT',
        url: apiUrl + `checklists/${listId}/items/${item.id}?access_token=${token}`,
        headers: {'Content-Type':'application/json'},
        data:{"checked":item.checked}
    }).catch(err => {
        var error = err.response.data.error
        var errMsg = error.statusCode + ' ' + err.message
        return dispatch(actionItemError(errMsg))
    });
}

export const deleteList = (listInfo) => async (dispatch) => {
    var token = localStorage.getItem('token')

    dispatch(requestItems())

    await axios({
        method: 'DELETE',
        url: apiUrl + `checklists/${listInfo.id}/items?access_token=${token}`,
        headers: {'Content-Type':'application/json'}
    }).catch(err => {
        var error = err.response.data.error
        var errMsg = error.statusCode + ' ' + err.message
        return dispatch(actionItemError(errMsg))
    });

    await axios({
        method: 'DELETE',
        url: apiUrl + `checklists/${listInfo.id}?access_token=${token}`,
        headers: {'Content-Type':'application/json'}
    }).catch(err => {
        var error = err.response.data.error
        var errMsg = error.statusCode + ' ' + err.message
        return dispatch(actionItemError(errMsg))
    });

    await sleep(500);
    window.location.href = "/"
}



export const saveItems = (listId,items) => async (dispatch) => {
    var token = localStorage.getItem('token')

    dispatch(requestItems())
    await sleep(300);

    console.log('--- delete items ---')
    await axios({
        method: 'DELETE',
        url: apiUrl + 'checklists/'+listId+'/items?access_token='+token,
        headers: {'Content-Type':'application/json'}
    }).catch(err => {
        var error = err.response.data.error
        console.log(error)
        var errMsg = error.statusCode + ' ' + err.message
        return dispatch(actionItemError(errMsg))
    });

    console.log('--- save items ---')
    await axios({
        method: 'POST',
        url: apiUrl + 'checklists/'+listId+'/items?access_token='+token,
        headers: {'Content-Type':'application/json'},
        data: items
    }).catch(err => {
        var error = err.response.data.error
        console.log(error)
        var errMsg = error.statusCode + ' ' + err.message
        return dispatch(actionItemError(errMsg))
    });

    return dispatch(getItems(listId))
}

export const switchType = (itemId,itemType) => async (dispatch) => {
    var token = localStorage.getItem('token')
    var userInfo = localStorage.getItem('userInfo')
    var uid = JSON.parse(userInfo).id
    itemType = (itemType==='item')?'cate':'item';

    dispatch(requestItems())
    console.log('--- switch type ---')
    var res = await axios({
        method: 'PUT',
        url: apiUrl + '/Customers/'+uid+'/items/'+itemId+'?access_token='+token,
        headers: {'Content-Type':'application/json'},
        data: {type:itemType}
    }).catch(err => {
        var error = err.response.data.error
        console.log(error)
        var errMsg = error.statusCode + ' ' + err.message
        return dispatch(actionItemError(errMsg))
    });

    if(res.status === 200){
        dispatch(getItems(res.data.checklistId))
    }
}

export const editListInfo = (listInfo) => async (dispatch) => {
    dispatch(setListInfo(listInfo))
}

export const saveListInfo = (listInfo) => async (dispatch) => {
    var token = localStorage.getItem('token')
    var userInfo = localStorage.getItem('userInfo')

    var postData = {
        title:listInfo.title,
        publish: listInfo.publish
    }

    await axios({
        method: 'PUT',
        url: apiUrl + `/Customers/${JSON.parse(userInfo).id}/checklists/${listInfo.id}?access_token=${token}`,
        headers: {'Content-Type':'application/json'},
        data: postData
    }).catch(err => {
        var error = err.response.data.error
        console.log(error)
        var errMsg = error.statusCode + ' ' + err.message
        return dispatch(actionItemError(errMsg))
    });

    dispatch(setListInfo(listInfo))
}


export const actionError = (err) => (dispatch) => {
    return dispatch({
        type: ActionTypes.CLIST_FAILURE,
        payload: err
    })
}

export const requestList = () => {
    return {type: ActionTypes.CLIST_REQUEST}
}

export const receiveList = (list) => {
    return {type: ActionTypes.CLIST_SUCCESS, payload: list}
}

export const actionItemError = (err) => (dispatch) => {
    return dispatch({
        type: ActionTypes.ITEMS_FAILURE,
        payload: err
    })
}

export const setListInfo = (listInfo) => (dispatch) => {
    return dispatch({
        type: ActionTypes.ITEMS_SET_LISTINFO,
        payload: listInfo,
    })
}

export const requestItems = () => {
    return {type: ActionTypes.ITEMS_REQUEST}
}

export const receiveItems = (items) => {
    return {type: ActionTypes.ITEMS_SUCCESS, payload: items}
}
