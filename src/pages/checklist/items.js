import React, { Component } from "react";
import { Button, Input, Modal, ModalBody, ModalFooter } from 'reactstrap';
import { initItems, getItems, createNewItem, switchType, deleteItem, deleteList,
    insertItemToStore, editItemInStore, checkItem, saveItems, editListInfo, saveListInfo } from "../checklist/actions";
import { connect } from 'react-redux';
import {Navigate} from "react-router-dom";

const mapState = state => {
    console.log('--- item map state ---')
    console.log(state.items)
    return {
        items: state.items
    }
}

const mapDispatch = (dispatch) => ({
    initItems: (listId) => dispatch(initItems(listId)),
    getItems: (listId) => dispatch(getItems(listId)),
    createNewItem: (item) => dispatch(createNewItem(item)),
    deleteItem: (item,index) => dispatch(deleteItem(item,index)),
    insertItemToStore: (items) => dispatch(insertItemToStore(items)),
    editItemInStore: (items) => dispatch(editItemInStore(items)),
    checkItem: (items,listId,itemId) => dispatch(checkItem(items,listId,itemId)),
    saveItems: (listId,items) => dispatch(saveItems(listId,items)),
    switchType: (itemId,itemType) => dispatch(switchType(itemId,itemType)),
    editListInfo: (listInfo) => dispatch(editListInfo(listInfo)),
    saveListInfo: (listInfo) => dispatch(saveListInfo(listInfo)),
    deleteList: (listInfo) => dispatch(deleteList(listInfo)),
})

class Items extends Component{
    constructor(props) {
        super(props);
        this.state = {
            itemName:'',
            modalOpen:false,
            errMsg:null
        };
        this.bindNewItemName = this.bindNewItemName.bind(this);
        this.createNewItem = this.createNewItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.insertItem = this.insertItem.bind(this);
        this.editItemName = this.editItemName.bind(this);
        this.editListTitle = this.editListTitle.bind(this);
        this.saveListTitle = this.saveListTitle.bind(this);
        this.publishList = this.publishList.bind(this);
        this.checkItem = this.checkItem.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }

    bindNewItemName(e){
        var itemName = e.target.value
        this.setState({itemName:itemName, errMsg:null})
    }

    createNewItem(e){
        e.preventDefault()
        var itemName = e.target.itemName.value
        if(itemName.length === 0){
            this.setState({errMsg:"Required"})
        }else{
            this.setState({errMsg:null})
            var item = {name:itemName, checklistId:this.props.listId}
            this.props.createNewItem(item)
        }
    }

    deleteItem(e){
        var index = parseInt(e.target.dataset.index)
        this.props.deleteItem(this.props.items.data, index)
    }

    insertItem(e){
        var index = parseInt(e.target.dataset.index)
        var items = this.props.items.data
        var currentItem = items[index]
        var item = {
            name:'',
            type:'item',
            checked:false,
            checklistId:currentItem.checklistId,
            customerId:currentItem.customerId,
            edit:true
        }
        items.splice(index+1,0,item)
        this.props.insertItemToStore(items)
    }

    saveItem(e){
        e.preventDefault()
        var index = e.target[0].dataset.index
        var value = e.target[0].value
        var items = this.props.items.data
        var currentItem = items[index]
        currentItem.name = value
        delete currentItem.edit
        items.filter(item => delete item.id)
        console.log(items)
        this.props.saveItems(currentItem.checklistId,items)
    }

    editItemName(e){
        var index = parseInt(e.target.dataset.index)
        var items = this.props.items.data
        items[index].edit = true
        // console.log(items)
        this.props.editItemInStore(items)
    }

    publishList(){
        var listInfo = this.props.items.listInfo
        listInfo.publish = !listInfo.publish
        this.props.saveListInfo(listInfo)
    }

    editListTitle(){
        var listInfo = this.props.items.listInfo
        listInfo.edit = true
        this.props.editListInfo(listInfo)
    }

    saveListTitle(e){
        e.preventDefault()
        var value = e.target[0].value
        var listInfo = this.props.items.listInfo
        listInfo.title = value
        delete listInfo.edit
        console.log(listInfo)
        this.props.saveListInfo(listInfo)
    }

    checkItem(e){
        var index = parseInt(e.target.dataset.index)
        console.log(index)
        var listId = this.props.items.listInfo.id
        var items = this.props.items.data
        // var itemId = items[index].id
        items[index].checked = !items[index].checked
        var item = items[index]
        // console.log(items)
        this.props.checkItem(items,listId,item)
    }


    toggleModal(){
        var modalOpen = !this.state.modalOpen
        this.setState({modalOpen:modalOpen})

        console.log(this.state)
    }


    componentDidMount() {
        console.log('--- item did mount ---')
        this.props.initItems(this.props.listId)
    }

    render() {
        const { items } = this.props

        if(items.listInfo.deleted){
            return (
                <Navigate to="/" />
            )
        }

        const ListTitle = () => {
            if(items.listInfo.id === null){
                return (
                    <div className="d-flex align-items-center justify-content-center">
                        <span className="spinner-border text-secondary spinner-border-sm" role="status"></span>
                        <span className="p-2">Loading Title...</span>
                    </div>
                )
            }else{
                if(items.listInfo.edit){
                    return (
                        <div className="btn-create-block d-flex justify-content-center">
                        <form onSubmit={(values)=>this.saveListTitle(values)}
                              className="col-12 col-md-6 d-flex align-items-center justify-content-between" name="ListTitleForm">
                            <Input type="text" className={this.state.errMsg} name="itemName" id="itemName"
                                   placeholder="List Title"
                                   required
                                   minLength={2}
                                   defaultValue={items.listInfo.title}
                                   innerRef={(input) => this.itemName = input}
                            />
                            <Button className="btn m-0 d-flex align-items-center justify-content-center" color="warning">
                                <span className="fa fa-save p-1"></span>Save
                            </Button>
                        </form>
                        </div>
                    )
                }
                return (
                    <h1 className="mb-4" onClick={items.listInfo.publish?()=>{}:this.editListTitle}>
                        {items.listInfo.title} <span className="fa fa-pencil fs-20 text-secondary"></span>
                    </h1>
                )
            }
        }

        const CreateNewItem = () => {
            if(!items.loading){
                return (
                    <div className="btn-create-block d-flex justify-content-center">
                        <form onSubmit={(values)=>this.createNewItem(values)}
                              className="col-12 offset-md-3 col-md-6 d-flex align-items-center justify-content-between" name="NewItemForm">
                            <Input type="text" className={this.state.errMsg} name="itemName" id="itemName"
                                   placeholder="Item/Category Name"
                                   required
                                   minLength={2}
                                   innerRef={(input) => this.itemName = input}
                            />
                            <Button className="btn btn-action m-0 d-flex justify-content-center" color="primary">Create<span className="d-none d-sm-block"> a new Item</span></Button>
                        </form>
                        <div className="d-none d-sm-block offset-md-3"></div>
                    </div>
                )
            }

        }

        const Loading = () => {
            if (items.loading) {
                return (
                    <div className="d-flex align-items-center justify-content-center">
                        <span className="spinner-border text-secondary spinner-border-sm" role="status"></span>
                        <span className="p-2">Loading...</span>
                    </div>
                )
            }
        }

        const ErrorMsg = () => {
            if(items.errMsg!==null){
                return (
                    <>
                        <p></p>
                        <div className="alert alert-danger" role="alert">
                            {items.errMsg}
                        </div>
                    </>
                )
            }
        }

        const List = () => {
            if(items.data.length===0 && !items.loading){
                return (
                    <div className={`p-5`}>
                        <img src="/images/empty.png" alt=""/>
                        <p className={`p-2`}></p>
                    </div>
                )
            }else{
                return (
                    <ul className="checklist col-12 col-md-6 list-group list-group-flush">
                        {items.data.map((item,index) => {
                            // insert item below or edit item
                            if(item.edit){
                                return (
                                    <li key={index} className={`type-${item.type} list-group-item d-flex justify-content-between align-items-center`}>
                                        <form onSubmit={(values)=>this.saveItem(values)}
                                              className="w-100 d-flex align-items-center justify-content-between" name="InsertItemForm">
                                            <Input data-index={index} type="text" name="itemName" className="w-75"
                                                   placeholder="Item/Category Name"
                                                   required
                                                   autoFocus
                                                   minLength={2}
                                                   defaultValue={item.name}
                                                   innerRef={(input) => this.itemName = input}
                                            />
                                            <div className="act-block d-flex justify-content-between align-items-center">
                                                <Button className="badge bg-primary m-0" color="primary">Save</Button>
                                                <span data-index={index} onClick={this.deleteItem} className="fa fa-minus-square fa-lg text-danger"></span>
                                            </div>
                                        </form>
                                    </li>
                                )
                            }

                            if(items.listInfo.publish){
                                return (
                                    <li key={index} className={`type-${item.type} list-group-item d-flex align-items-center`}>
                                        <div className="d-flex align-items-center">
                                            <i data-index={index} onClick={item.type==="cate"?()=>{}:this.checkItem}
                                                className={`fa ${item.checked?'text-success fa-check-circle':'fw-light text-secondary fa-circle-o'} fa-lg p-1`}></i>
                                            <h5 className="m-0 p-1" data-index={index} onClick={item.type==="cate"?()=>{}:this.checkItem}>
                                                {item.name}
                                            </h5>
                                        </div>
                                    </li>
                                )
                            }

                            return (
                                <li key={index} className={`type-${item.type} list-group-item d-flex justify-content-between align-items-center`}>
                                    <h5 data-index={index} onClick={this.editItemName}>{item.name}</h5>
                                    <div className="act-block ">
                                        <span onClick={()=>this.props.switchType(item.id,item.type)} className={`badge bg-${item.type} text-uppercase`}>{item.type}</span>
                                        <span data-index={index} onClick={this.deleteItem} className="fa fa-minus-square fa-lg text-danger"></span>
                                        <span data-index={index} onClick={this.insertItem} className="fa fa-plus-square fa-lg text-success"></span>
                                    </div>
                                </li>
                            )
                        })}
                    </ul>
                )
            }
        }

        const SwitchToggle = () => {
            if(!items.loading){
                if(items.listInfo.publish){
                    return (
                        <div className="col-12 col-md-6">
                            <div className="btn-edit-list d-flex justify-content-end align-items-center" onClick={this.publishList}>
                                <i className="fa fa-arrow-right p-2"></i> Edit List
                            </div>
                        </div>
                    )
                }else{
                    if(items.data.length>0){
                        return (
                            <div className="col-12 col-md-6">
                                <p className="p-3"></p>
                                <Button onClick={this.publishList}
                                        className="btn btn-action mb-4 w-100" color="success">
                                    Publish and Use
                                </Button>
                                <Button onClick={this.toggleModal}
                                        className="btn btn-action mb-4 w-100" color="danger">Delete List</Button>
                            </div>

                        )
                    }else {
                        return ('')
                    }

                }
            }
        }


        const ConfirmDeleteModal = () => {
            return (
                <Modal isOpen={this.state.modalOpen} toggle={this.toggleModal} className="modal-dialog-centered">
                    <ModalBody className="fs-18">
                        Are you sure to <span className="text-danger">Delete</span> the whole Checklist, click Delete to delete the list, click cancel back to screen.
                    </ModalBody>

                    <ModalFooter>
                        <Button onClick={this.toggleModal}
                            className="btn" color="secondary">Cancel</Button>
                        <Button onClick={() => {
                            this.toggleModal();
                            this.props.deleteList(this.props.items.listInfo);
                        }}
                                className="btn" color="danger">Delete</Button>
                    </ModalFooter>
                </Modal>
            )
        }

        return(
            <div className={`publish-${items.listInfo.publish} checklist-container container d-flex flex-column align-items-center text-center p-4`}>
                <ListTitle />

                <List />

                <Loading />

                <CreateNewItem />

                <SwitchToggle />

                <ConfirmDeleteModal />

                <ErrorMsg />
            </div>
        )
    }
}

// Home = reduxForm({
//     form: 'checklist',
//     validate
// })(Home);

export default connect(mapState, mapDispatch)(Items)

