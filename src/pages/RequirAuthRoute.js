import { Navigate , useLocation } from "react-router-dom"

export default function RequirAuthRoute({children}) {
    //获取到locationStorage中的token
    const token = localStorage.getItem('token')

    //获取location
    const { pathname} = useLocation()
    if(!token){
        return <Navigate to="/users/login" state={{returnURL:pathname}}></Navigate>
    }
    //如果存在 则渲染标签中的内容
    return children
}
