import React, { Component } from "react";
import { Modal, ModalHeader, ModalBody, Button, Input } from 'reactstrap';
import { createList, getList, toggleModal } from "../checklist/actions";
import { NavLink } from "react-router-dom";
import { connect } from 'react-redux';

const mapState = state => {
    console.log(state.checklist)
    return {
        checklist: state.checklist,
    }
}

const mapDispatch = (dispatch) => ({
    handleSubmit: (values) => dispatch(createList(values)),
    toggleModal: (status) => dispatch(toggleModal(status)),
    getList: () => dispatch(getList())
})

class Home extends Component{
    componentDidMount() {
        this.props.getList()
    }

    handleSubmit(event) {
        event.preventDefault()
        console.log(event.target[0].value)
        // return false;
        this.createList(event.target[0].value);
        return false;
    }

    render() {
        const { toggleModal, handleSubmit, checklist } = this.props

        const CreateBtn = () => {
            var token = localStorage.getItem('token')
            if(token!==null){
                return (
                    <Button onClick={()=>toggleModal(checklist.modalOpenStatus)} className="btn btn-action" color="primary">
                        {checklist.data.length===0?"Create your first Checklist":"Create a new Checklist"}
                    </Button>
                )
            }else{
                return (
                    <div className="fs-20">You are not authenticated, please login first.</div>
                )
            }

        }

        const FormModal = () => {
            return (
                <Modal isOpen={checklist.modalOpenStatus}>
                    <ModalHeader toggle={()=>toggleModal(checklist.modalOpenStatus)}>Create a new Checklist</ModalHeader>
                    <ModalBody>
                        <form onSubmit={(values) => {values.preventDefault(); handleSubmit(values)}}>
                            <div className="p-3">
                                <Input placeholder="Please fill in title"
                                      innerRef={(input) => this.title = input} type="text" id="title" name="title" />

                                <div className="text-end pt-3 d-flex justify-content-between">
                                    <div className="text-danger">{checklist.errMsg}</div>
                                    <Button color="primary">
                                        <span className="fa fa-save"></span> Save
                                    </Button>
                                </div>
                            </div>
                        </form>
                    </ModalBody>
                </Modal>
            )
        }

        const Loading = () => {
            if (checklist.loading) {
                return (
                    <div className="d-flex align-items-center justify-content-center">
                        <span className="spinner-border text-secondary spinner-border-sm" role="status"></span>
                        <span className="p-2">Loading...</span>
                    </div>
                )
            }
        }

        const ErrorMsg = () => {
            if(checklist.errMsg!==null && !checklist.modalOpenStatus){
                return (
                    <>
                        <p></p>
                        <div className="alert alert-danger" role="alert">
                            {checklist.errMsg}
                        </div>
                    </>
                )
            }
        }

        const List = () => {
            var token = localStorage.getItem('token')
            if(token && checklist.data.length>0){
                return (
                    <ul className="checklist list-group list-group-flush">
                        {checklist.data.map((item) => {
                            return (
                                <li key={item.id} className="list-group-item">
                                    <NavLink to={`/checklist/${item.id}`}>{item.title}</NavLink>
                                </li>
                            )
                        })}
                    </ul>
                )
            }
        }

        return(
            <div className="container d-flex flex-column justify-content-center align-items-center text-center p-4">
                <br/>
                <h1>Welcome to Online Packing Checklist</h1>
                <p className="text-secondary">I hope this tool will help you to manage your checklist well</p>
                <p></p>
                <CreateBtn />
                <p></p>
                <Loading />
                <List />
                <ErrorMsg />

                <FormModal />
            </div>
        )
    }
}

// Home = reduxForm({
//     form: 'checklist',
//     validate
// })(Home);

export default connect(mapState, mapDispatch)(Home)

