import * as ActionTypes from './ActionTypes';

export const checklist = (state = {
    loading: true,
    errMsg: null,
    modalOpenStatus: false,
    data: [],
    pageInfo: {totalNum:0, pageNum:0, currentPage:1, perPage: 10}
}, action) => {
    switch (action.type) {
        case ActionTypes.MODAL_TOGGLE:
            return {...state, modalOpenStatus: action.payload, loading: false, errMsg: null};

        case ActionTypes.CLIST_REQUEST:
            return {...state, loading: true, errMsg: null};

        case ActionTypes.CLIST_FAILURE:
            return {...state, loading: false, errMsg: action.payload};

        case ActionTypes.CLIST_SUCCESS:
            return {...state, loading: false, errMsg: null, data:action.payload};

        default:
            return state;
    }
}
