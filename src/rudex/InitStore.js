import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk";
import logger from 'redux-logger';
import { reducer as formReducer } from 'redux-form'
import { UserInfo } from "./UserInfo";
import { checklist } from "./checklist";
import { items } from "./items";

export const InitStore = () => {
    const store = createStore(
        combineReducers({
            form: formReducer,
            UserInfo: UserInfo,
            checklist: checklist,
            items: items,
        }),
        applyMiddleware(thunk, logger)
    );
    return store;
}
