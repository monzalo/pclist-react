export const INIT_DATA = 'INIT_DATA';
export const INIT_LOADING = 'INIT_LOADING';
export const INIT_FAILED = 'INIT_FAILED';

export const MODAL_TOGGLE = 'MODAL_TOGGLE';

export const CLIST_REQUEST = 'CLIST_REQUEST';
export const CLIST_FAILURE = 'CLIST_FAILURE';
export const CLIST_SUCCESS = 'CLIST_SUCCESS';

export const ITEMS_SET_LISTINFO = 'ITEMS_SET_LISTINFO';
export const ITEMS_SET_PAGEINFO = 'ITEMS_SET_PAGEINFO';
export const ITEMS_REQUEST = 'ITEMS_REQUEST';
export const ITEMS_FAILURE = 'ITEMS_FAILURE';
export const ITEMS_SUCCESS = 'ITEMS_SUCCESS';
export const DEL_LIST_SUCCESS = 'DEL_LIST_SUCCESS';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE';
