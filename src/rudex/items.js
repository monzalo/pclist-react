import * as ActionTypes from './ActionTypes';

export const items = (state = {
    listInfo: {id:null, title: null, publish: false, deleted: false},
    loading: true,
    errMsg: null,
    data: [],
    pageInfo: {totalNum:0, pageNum:0, currentPage:1, perPage: 10}
}, action) => {
    switch (action.type) {
        case ActionTypes.ITEMS_SET_LISTINFO:
            return {...state, listInfo: action.payload};

        case ActionTypes.ITEMS_SET_PAGEINFO:
            return {...state, pageInfo: action.payload};

        case ActionTypes.ITEMS_REQUEST:
            return {...state, loading: true, errMsg: null};

        case ActionTypes.ITEMS_FAILURE:
            return {...state, loading: false, errMsg: action.payload};

        case ActionTypes.ITEMS_SUCCESS:
            return {...state, loading: false, errMsg: null, data:action.payload};

        case ActionTypes.DEL_LIST_SUCCESS:
            return {...state, loading: false, errMsg: null, listInfo:action.payload};

        default:
            return state;
    }
}
