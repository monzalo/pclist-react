import * as ActionTypes from './ActionTypes';

export const UserInfo = (state = {
        loading: false,
        errMsg: null,
        data: []
    }, action) => {
    switch(action.type) {
        case ActionTypes.LOGIN_SUCCESS:
            return {...state, loading: false, errMsg: null, data:action.payload};

        case ActionTypes.LOGOUT_REQUEST:
            return {...state, loading: true, errMsg: null, data:[]};

        case ActionTypes.LOGOUT_FAILURE:
            return {...state, loading: false, errMsg: action.payload, data:[]};

        case ActionTypes.LOGOUT_SUCCESS:
            return {...state, loading: false, errMsg: null, data:[]};

        default:
            return state;
    }
}
